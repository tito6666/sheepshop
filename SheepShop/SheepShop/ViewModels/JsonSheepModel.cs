﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SheepShop.ViewModels
{
    public class JsonSheepModel
    {
        public string name { get; set; }

        public string age { get; set; }

        public string ageLastShaved { get; set; }

        public JsonSheepModel(string name, string age, string ageLastShaved)
        {
            this.name = name;
            this.age = age;
            this.ageLastShaved = ageLastShaved;
        }

        public JsonSheepModel() { }
    }
}