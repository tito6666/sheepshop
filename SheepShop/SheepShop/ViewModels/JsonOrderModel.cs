﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SheepShop.ViewModels
{
    public class JsonOrderModel
    {
        [Display(Name = "Customer")]
        public string customer { get; set; }

        [Display(Name = "Order")]
        public Order order { get; set; }

    }

    public class Order 
    {
        [Display(Name = "Milk")]
        public string milk { get; set; }

        [Display(Name = "Skins")]
        public string skins { get; set; }
    }
}