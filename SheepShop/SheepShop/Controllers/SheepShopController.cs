﻿using Newtonsoft.Json;
using SheepShop.Models;
using SheepShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Xml;
using System.Xml.Linq;

namespace SheepShop.Controllers
{
    public class SheepShopController : Controller
    {
        //
        // GET: /SheepShop/

        private static decimal zeroZeroOne = 0.01M;
        private static decimal zeroZeroThree = 0.03M;

        public ActionResult Index()
        {
            SheepModel model = new SheepModel();
            return View(model);
        }
        //Method that displays output
        public ActionResult SheepXml(int T)
        {
            StockModel model = new StockModel();
            model = GetStockAndHeard(T);
            return View("Output", model);
        }
        //Method that gets stock and herd data
        public StockModel GetStockAndHeard(int T)
        {
            decimal stockMilk = 0;
            var woolCount = 0;
            if (T > 0)
            {
                List<SheepModel> listSheep = new List<SheepModel>();
                listSheep = SheepList();
                foreach (var sheep in listSheep)
                {
                    decimal sheepAge = Decimal.Parse(sheep.Age);
                    sheepAge = sheepAge * 100;
                    if (sheepAge < 1000) { stockMilk = stockMilk + MilkPerSheep(sheepAge, T); }
                    if (sheepAge > 100 && sheepAge < 1000)
                    {
                        woolCount++;
                        woolCount = woolCount + CanShaveSheepCount(sheepAge, T);
                    }
                    else if (sheepAge >= 0 && sheepAge <= 100)
                    {
                        woolCount = woolCount + CanShaveSheepCount(sheepAge, T);
                    }
                    else if (sheepAge >= 1000)
                    {

                    }
                    else
                    {
                        woolCount = 0;
                    }
                }
                StockModel model = new StockModel();
                model.MilkLiters = stockMilk;
                model.SkinsOfWool = woolCount;
                model.allSheeps = CountSheepAge(listSheep, T);
                model.T = T;

                return model;
            }
            else
            {
                StockModel model = new StockModel();
                model.T = 0;
                return model;
            }
        }
        //method that gets all sheeps from xml
        public List<SheepModel> SheepList() {

            List<SheepModel> listSheep = new List<SheepModel>();
            XDocument xdocument = XDocument.Load("C:\\Users\\Slaven\\Documents\\Visual Studio 2013\\Projects\\SheepShop\\SheepShop\\App_Data\\herd.xml");
            IEnumerable<XElement> heard = xdocument.Root.Elements();
            foreach (var sheep in heard)
            {
                SheepModel model = new SheepModel();
                model.Name = sheep.Attribute("name").Value;
                model.Age = sheep.Attribute("age").Value;
                model.Sex = sheep.Attribute("sex").Value;
                listSheep.Add(model);
            }
            return listSheep;

        }
        //method for getting stock data with request GET /sheep-shop/stock/T
        [HttpGet]
        public ActionResult Stock(int T) 
        {
            var model = GetStockAndHeard(T);
            JsonStockModel jsonModel = new JsonStockModel();
            jsonModel.milk = model.MilkLiters;
            jsonModel.skins = model.SkinsOfWool;
            var jsonObj = JsonConvert.SerializeObject(jsonModel);
            return Json(jsonObj,JsonRequestBehavior.AllowGet);
        }
        //method for getting stock data with request GET /sheep-shop/herd/T
        [HttpGet]
        public ActionResult Herd(int T) 
        {
            if (T > 0)
            {
                var listSheepXml = SheepList();
                decimal lastShaved = 0;
                List<JsonSheepModel> listSheep = new List<JsonSheepModel>();
                foreach (var sheep in listSheepXml)
                {
                    decimal sheepAge = Decimal.Parse(sheep.Age);
                    JsonSheepModel sheepModel = new JsonSheepModel();
                    sheepAge = sheepAge * 100;

                    if (sheepAge > 100 && sheepAge < 1000)
                    {
                        var age = AgeSheepLastShaved(sheepAge, T);
                        lastShaved = age == 0 ? Decimal.Parse(sheep.Age) : age;
                    }
                    else if (sheepAge >= 0 && sheepAge <= 100)
                    {
                        lastShaved = AgeSheepLastShaved(sheepAge, T);
                    }
                    else if (sheepAge >= 1000)
                    {

                    }
                    else
                    {
                        lastShaved = 0;
                    }
                    decimal sheepAgeParsed = Decimal.Parse(sheep.Age);
                    decimal timeDevided = (decimal)(T / 100M);
                    var sheepCurrentAge = sheepAgeParsed + timeDevided;

                    sheepModel.name = sheep.Name;
                    if (sheepCurrentAge >= 10)
                    {
                        sheepModel.age = "died";
                    }
                    else 
                    { 
                        sheepModel.age = sheepCurrentAge.ToString();
                    }
                    sheepModel.ageLastShaved = lastShaved.ToString();
                    listSheep.Add(sheepModel);
                }
                JsonHerdModel jsonModel = new JsonHerdModel();
                jsonModel.herd = listSheep;
                var jsonObj = JsonConvert.SerializeObject(jsonModel);
                return Json(jsonObj, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                var jsonObj = "At least one day didn't elapsed";
                return Json(jsonObj, JsonRequestBehavior.AllowGet);
            
            }
        }
        //method for posting order POST /sheep-shop/order/T
        [HttpPost]
        public ActionResult Order(int T, string jsModel) 
        {
            if (T > 0)
            {
                JsonOrderModel orderModel = new JsonOrderModel();
                orderModel = JsonConvert.DeserializeObject<JsonOrderModel>(jsModel);
                var customer = orderModel.customer;
                var order = orderModel.order;
                var milkOrder = Decimal.Parse(order.milk, CultureInfo.InvariantCulture);
                var skinsOrder = Decimal.Parse(order.skins);

                StockModel stockModel = new StockModel();
                stockModel = GetStockAndHeard(T);
                var milkStock = stockModel.MilkLiters;
                var skinsStock = stockModel.SkinsOfWool;
                JsonStockModel jsonStockModel = new JsonStockModel();

                if (milkStock >= milkOrder && skinsStock >= skinsOrder)
                {
                    jsonStockModel.milk = milkOrder;
                    jsonStockModel.skins = skinsOrder;
                    var jsonObj = JsonConvert.SerializeObject(jsonStockModel);
                    Response.StatusCode = 201;
                    return Json(new { success = true, data = jsonObj });
                }
                else if (milkStock >= milkOrder && skinsStock != skinsOrder)
                {
                    JsonMilkOrder jsonMilkModel = new JsonMilkOrder();
                    jsonMilkModel.milk = milkOrder;
                    var jsonObj = JsonConvert.SerializeObject(jsonMilkModel);
                    Response.StatusCode = 206;
                    return Json(new { success = true, data = jsonObj });
                }
                else if (milkStock != milkOrder && skinsStock >= skinsOrder)
                {
                    JsonSkinsOrder jsonSkinsModel = new JsonSkinsOrder();
                    jsonSkinsModel.skins = skinsOrder;
                    var jsonObj = JsonConvert.SerializeObject(jsonSkinsModel);
                    Response.StatusCode = 206;
                    return Json(new { success = true, data = jsonObj });
                }
                else
                {
                    Response.StatusCode = 404;
                    return Json(new { success = true});
                }
            }
            else
            {
                return Json(new { success = false, responseText = "At least one day didn't elapsed" });
            }
        }
        //method that counts milk per one sheep
        public decimal MilkPerSheep(decimal sheepAge, int T)
        {
            decimal sumMilkPerSheep = 0;
            var sumSheepAgeT = sheepAge + T;
            if (sheepAge + T >= 1000)
                sumSheepAgeT = 1000;
            for (var i = sheepAge; i < sumSheepAgeT; i++)
            {
                sumMilkPerSheep = sumMilkPerSheep + (50 - (i * zeroZeroThree));
            }
            return sumMilkPerSheep;
        }
        //method that calculates when sheep can be shaved
        public int CanShaveSheepCount(decimal sheepAge, int T)
        {
            int woolCount = 0;
            decimal canShaveSheep = 0;
            var sumSheepAgeT = sheepAge + T;
            if (sheepAge + T >= 1000)
                sumSheepAgeT = 1000;
            for (var i = sheepAge; i < sumSheepAgeT; i++)
            {
                canShaveSheep = 8 + (i * zeroZeroOne);
                if (canShaveSheep + 2 <= T && (canShaveSheep % 1) == 0 && i != 0)
                {
                    woolCount++;
                }
            }
            return woolCount;
        }
        //method that counts when the sheep was last shaved
        public decimal AgeSheepLastShaved(decimal sheepAge, int T)
        {
            decimal ageSheepShaved = 0;
            decimal canShaveSheep = 0;
            var sumSheepAgeT = sheepAge + T;
            if (sheepAge + T >= 1000)
                sumSheepAgeT = 1000;
            for (var i = sheepAge; i < sumSheepAgeT; i++)
            {
                canShaveSheep = 8 + (i * zeroZeroOne);
                if (canShaveSheep + 2 <= T && (canShaveSheep % 1) == 0 && i != 0)
                {
                    var sheepCurrentAge = i + canShaveSheep + 1;
                    ageSheepShaved = sheepCurrentAge/100;
                }
            }
            return ageSheepShaved;
         }
        //method that counts sheep age
        public List<SheepModel> CountSheepAge(List<SheepModel> list, int T) 
        {
            List<SheepModel> listSheep = new List<SheepModel>();
            foreach (var sheep in list)
            {
                var sheepAge = Decimal.Parse(sheep.Age);
                decimal timeDevided = (decimal)(T / 100M);
                var ifSheepDead = sheepAge + timeDevided;
                sheep.Age = (sheepAge + timeDevided).ToString();
                if (ifSheepDead >= 10)
                    sheep.Age = "dead";
                listSheep.Add(sheep);
            }
            return listSheep;
        }

       
                
        }

    }



