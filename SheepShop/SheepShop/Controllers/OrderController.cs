﻿using Newtonsoft.Json;
using SheepShop.Models;
using SheepShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SheepShop.Controllers
{
    public class OrderController : Controller
    {
        //
        // GET: /Order/

        public ActionResult Index()
        {
            OrderModel model = new OrderModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Order(int Time, OrderModel orderModel)
        {
            if (Time > 0)
            {
                var customer = orderModel.Customer;
                var order = orderModel.Order;
                var milkOrder = order.Milk;
                var skinsOrder = order.Skins;

                StockModel stockModel = new StockModel();
                SheepShopController controller = new SheepShopController();
                stockModel = controller.GetStockAndHeard(Time);
                var milkStock = stockModel.MilkLiters;
                var skinsStock = stockModel.SkinsOfWool;
                JsonStockModel jsonStockModel = new JsonStockModel();

                if (milkStock >= milkOrder && skinsStock >= skinsOrder)
                {
                    Response.StatusCode = 201;
                    return Json(new { success = true});
                }
                else if (milkStock >= milkOrder && skinsStock != skinsOrder)
                {
                    JsonMilkOrder jsonMilkModel = new JsonMilkOrder();
                    jsonMilkModel.milk = milkOrder;
                    var jsonObj = JsonConvert.SerializeObject(jsonMilkModel);
                    Response.StatusCode = 206;
                    return Json(new { success = true, data = jsonObj });
                }
                else if (milkStock != milkOrder && skinsStock >= skinsOrder)
                {
                    JsonSkinsOrder jsonSkinsModel = new JsonSkinsOrder();
                    jsonSkinsModel.skins = skinsOrder;
                    var jsonObj = JsonConvert.SerializeObject(jsonSkinsModel);
                    Response.StatusCode = 206;
                    return Json(new { success = true, data = jsonObj });
                }
                else
                {
                    Response.StatusCode = 404;
                    return Json(new { success = true });
                }
            }
            else
            {
                return Json(new { success = false, responseText = "At least one day didn't elapsed" });
            }
        }
 
    }
}
