﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SheepShop.Models
{
    public class OrderModel
    {
       
        [Display(Name = "Customer")]
        public string Customer { get; set; }

        [Display(Name = "Order")]
        public OrderOrder Order { get; set; }

    
        [Display(Name = "Time")]
        public int Time { get; set; }
    }

    public class OrderOrder 
    {
       
        [Display(Name = "Milk")]
        public decimal Milk { get; set; }

      
        [Display(Name = "Skins")]
        public decimal Skins { get; set; }
    
    }
}