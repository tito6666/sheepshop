﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SheepShop.Models
{
    public class StockModel
    {
        public decimal MilkLiters { get; set; }

        public int SkinsOfWool { get; set; }

        public List<SheepModel> allSheeps { get; set; }

        public int T { get; set; }

        public string AgeLastShaved { get; set; }

    }
}