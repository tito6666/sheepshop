﻿using SheepShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheepShop.UnitTests
{
    public class SheepEqualityComparer : IEqualityComparer<SheepModel>
    {
        public bool Equals(SheepModel x, SheepModel y)
        {
            if (object.ReferenceEquals(x, y)) return true;

            if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null)) return false;

            return x.Name == y.Name && x.Age == y.Age && x.Sex == x.Sex;
        }

        public int GetHashCode(SheepModel obj)
        {
            if (object.ReferenceEquals(obj, null)) return 0;

            int hashCodeName = obj.Name == null ? 0 : obj.Name.GetHashCode();
            int hasCodeAge = obj.Age.GetHashCode();
            int hasCodeSex = obj.Sex.GetHashCode();

            return hashCodeName * hasCodeAge * hasCodeSex;
        }
    }
}
