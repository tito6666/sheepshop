﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SheepShop.Controllers;
using SheepShop.Models;
using SheepShop.ViewModels;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SheepShop.UnitTests
{
    public class SheepShopControllerTest
    {
        
        [Test]
        public void GetStockAndHerdTest1()
        {
            int T = 13;
            StockModel testModel = new StockModel();
            List<SheepModel> allSheeps = new List<SheepModel>();
            SheepModel sheep1 = new SheepModel("Betty-1", "4,13", "f");
            SheepModel sheep2 = new SheepModel("Betty-2", "8,13", "f");
            SheepModel sheep3 = new SheepModel("Betty-3", "9,63", "f");
           
            allSheeps.Add(sheep1);
            allSheeps.Add(sheep2);
            allSheeps.Add(sheep3);
            testModel.MilkLiters = 1104.480M;
            testModel.SkinsOfWool = 3;
            testModel.allSheeps = allSheeps;
            testModel.T = T;
            testModel.AgeLastShaved = null;

            SheepShopController controller = new SheepShopController();
            StockModel model = new StockModel();
            model = controller.GetStockAndHeard(T);
       
            Assert.AreEqual(model.MilkLiters, testModel.MilkLiters);
            Assert.AreEqual(model.SkinsOfWool, testModel.SkinsOfWool);
            Assert.IsTrue(model.allSheeps.SequenceEqual(testModel.allSheeps, new SheepEqualityComparer()));
            Assert.AreEqual(model.AgeLastShaved, testModel.AgeLastShaved);
        }

        [Test]
        public void GetStockAndHerdTest2()
        {
            int T = 14;
            StockModel testModel = new StockModel();
            List<SheepModel> allSheeps = new List<SheepModel>();
            SheepModel sheep1 = new SheepModel("Betty-1", "4,14", "f");
            SheepModel sheep2 = new SheepModel("Betty-2", "8,14", "f");
            SheepModel sheep3 = new SheepModel("Betty-3", "9,64", "f");

            allSheeps.Add(sheep1);
            allSheeps.Add(sheep2);
            allSheeps.Add(sheep3);
            testModel.MilkLiters = 1188.81M;
            testModel.SkinsOfWool = 4;
            testModel.allSheeps = allSheeps;
            testModel.T = T;
            testModel.AgeLastShaved = null;

            SheepShopController controller = new SheepShopController();
            StockModel model = new StockModel();
            model = controller.GetStockAndHeard(T);

            Assert.AreEqual(model.MilkLiters, testModel.MilkLiters);
            Assert.AreEqual(model.SkinsOfWool, testModel.SkinsOfWool);
            Assert.IsTrue(model.allSheeps.SequenceEqual(testModel.allSheeps, new SheepEqualityComparer()));
            Assert.AreEqual(model.AgeLastShaved, testModel.AgeLastShaved);
        }

        [Test]
        public void SheepListTest() 
        {
            List<SheepModel> allSheepsTest = new List<SheepModel>();
            SheepModel sheep1 = new SheepModel("Betty-1", "4", "f");
            SheepModel sheep2 = new SheepModel("Betty-2", "8", "f");
            SheepModel sheep3 = new SheepModel("Betty-3", "9,5", "f");
            allSheepsTest.Add(sheep1);
            allSheepsTest.Add(sheep2);
            allSheepsTest.Add(sheep3);
            SheepShopController controller = new SheepShopController();
            List<SheepModel> allSheeps = new List<SheepModel>();
            allSheeps = controller.SheepList();
            Assert.IsTrue(allSheeps.SequenceEqual(allSheepsTest, new SheepEqualityComparer()));
        }

        [Test]
        public void StockTest()
        {
            int T = 13;
            JsonStockModel jsonModel = new JsonStockModel();
            jsonModel.milk = 1104.480M;
            jsonModel.skins = 3.0M;
            var serializer = new JavaScriptSerializer();
            var output = serializer.Serialize(jsonModel);     
    
            SheepShopController controller = new SheepShopController();
            var jsonObj = controller.Stock(T) as JsonResult;
            Assert.AreEqual(output, jsonObj.Data); 
        }

        [Test]
        public void HerdTest1()
        {
            int T = 13;
            SheepShopController controller = new SheepShopController();
            var jsonObj = controller.Herd(T) as JsonResult;
            List<JsonSheepModel> herd = new List<JsonSheepModel>();
            JsonSheepModel sheep1 = new JsonSheepModel("Betty-1", "4,13","4");
            JsonSheepModel sheep2 = new JsonSheepModel("Betty-2", "8,13", "8");
            JsonSheepModel sheep3 = new JsonSheepModel("Betty-3", "9,63", "9,5");
            herd.Add(sheep1);
            herd.Add(sheep2);
            herd.Add(sheep3);
            JsonHerdModel model = new JsonHerdModel();
            model.herd = herd;

            var serializer = new JavaScriptSerializer();
            var output = serializer.Serialize(model);  

            Assert.AreEqual(jsonObj.Data, output); 
        }

        [Test]
        public void HerdTest2()
        {
            int T = 14;
            SheepShopController controller = new SheepShopController();
            var jsonObj = controller.Herd(T) as JsonResult;
            List<JsonSheepModel> herd = new List<JsonSheepModel>();
            JsonSheepModel sheep1 = new JsonSheepModel("Betty-1", "4,14", "4,13");
            JsonSheepModel sheep2 = new JsonSheepModel("Betty-2", "8,14", "8");
            JsonSheepModel sheep3 = new JsonSheepModel("Betty-3", "9,64", "9,5");
            herd.Add(sheep1);
            herd.Add(sheep2);
            herd.Add(sheep3);
            JsonHerdModel model = new JsonHerdModel();
            model.herd = herd;

            var serializer = new JavaScriptSerializer();
            var output = serializer.Serialize(model);

            Assert.AreEqual(jsonObj.Data, output);
        }

        [Test]
        public void HerdTest3()
        {
            int T = 0;
            SheepShopController controller = new SheepShopController();
            var jsonObj = controller.Herd(T) as JsonResult;

            Assert.AreEqual(jsonObj.Data, "At least one day didn't elapsed");
        }


        [Test]
        public void SumMilkPerSheep() 
        { 
            decimal sheepAge = 400.0M;
            int T = 13;
            SheepShopController controller = new SheepShopController();
            var sumMilkSheep =  controller.MilkPerSheep(sheepAge,T);
            var sumExpected = 491.66M;
            Assert.AreEqual(sumExpected,sumMilkSheep);
        }
        //how much wools will get for T days and sheep age
        [Test]
        public void CanShaveSheepCount()
        {
            decimal sheepAge = 400.0M;
            int T = 14;
            int count = 1;
            SheepShopController controller = new SheepShopController();
            var canShaveSheepCount = controller.CanShaveSheepCount(sheepAge,T);
            Assert.AreEqual(count,canShaveSheepCount);

        }
        [Test]
        public void AgeSheepLastShavedTest() 
        {
            decimal sheepAge = 400.0M;
            int T = 14;
            SheepShopController controller = new SheepShopController();
            var ageLastShaved = controller.AgeSheepLastShaved(sheepAge, T);
            var lastShaved = 4.13M;
            Assert.AreEqual(ageLastShaved,lastShaved);
        }
        [Test]
        public void CountSheepAgeTest() 
        {
            int T = 13;
            List<SheepModel> listSheep = new List<SheepModel>();
            SheepModel sheep1 = new SheepModel("Betty-1", "4", "f");
            SheepModel sheep2 = new SheepModel("Betty-2", "8", "f");
            SheepModel sheep3 = new SheepModel("Betty-3", "9,5", "f");
            listSheep.Add(sheep1);
            listSheep.Add(sheep2);
            listSheep.Add(sheep3);
            List<SheepModel> listSheepExpected = new List<SheepModel>();
            SheepModel sheep4 = new SheepModel("Betty-1", "4,13", "f");
            SheepModel sheep5 = new SheepModel("Betty-2", "8,13", "f");
            SheepModel sheep6 = new SheepModel("Betty-3", "9,63", "f");
            listSheepExpected.Add(sheep4);
            listSheepExpected.Add(sheep5);
            listSheepExpected.Add(sheep6);
            SheepShopController controller = new SheepShopController();
            var ageLastShaved = controller.CountSheepAge(listSheep, T);
            Assert.IsTrue(ageLastShaved.SequenceEqual(listSheepExpected, new SheepEqualityComparer()));
        }

    }
}
